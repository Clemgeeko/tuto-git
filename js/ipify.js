function updateDisplay() {
    ipDisplay.innerHTML = '&#x231B;'; // Affichage d'un sablier

    fetch('https://api.ipify.org?format=json').then(function (response) {
        if (response.ok) {
            response.json().then(function (json) {
                ipDisplay.textContent = json.ip;
            });
        } else {
            console.log("Echec d'obtention de l'adresse IP avec l 'erreur " + response.status + ": " + response.statusText);
        }
    });
}

var ipDisplay = document.querySelector('#ip-display');

updateDisplay();